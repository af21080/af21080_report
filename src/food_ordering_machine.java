import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class food_ordering_machine {
    private JPanel root;
    private JButton riceOmeletButton;
    private JButton sushiButton;
    private JButton curryAndRiceButton;
    private JButton pastaButton;
    private JButton takoyakiButton;
    private JButton odenButton;
    private JButton checkOutButton;
    private JTextArea textArea1;
    private JLabel Total;
    int price;
    int a;



    void order(String food) {
        int confirmation=JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);


        if(confirmation==0) {
            JOptionPane.showMessageDialog(null,"Thank you for ordering " + food + "! It will be served as soon as possible.");

            String currentText1 =textArea1.getText();
            textArea1.setText(currentText1+food+"\n");

            a+=price;
            Total.setText("Total   "+a+"yen");

        }
    }


    public food_ordering_machine(){
        Total.setText("Total   "+0+"yen");
        riceOmeletButton.setIcon(new ImageIcon(
                this.getClass().getResource("omu.jpg")
        ));
        curryAndRiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("curry.jpg")
        ));
        takoyakiButton.setIcon(new ImageIcon(
                this.getClass().getResource("tako.jpg")
        ));
        sushiButton.setIcon(new ImageIcon(
                this.getClass().getResource("sushi.jpg")
        ));
        pastaButton.setIcon(new ImageIcon(
                this.getClass().getResource("pasta.jpg")
        ));
        odenButton.setIcon(new ImageIcon(
                this.getClass().getResource("oden.jpg")
        ));

        riceOmeletButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                price=800;
                order("Rice Omelet");
            }

        });
        curryAndRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=700;
                order("Curry and rice");
            }
        });
        takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=500;
                order("Takoyaki");
            }
        });
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=1500;
                order("Sushi");
            }
        });
        pastaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=1000;
                order("Pasta");
            }
        });
        odenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=600;
                order("Oden");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation2=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);


                if(confirmation2==0) {
                    JOptionPane.showMessageDialog(null, "Thank you.The total price is "+a+" yen.");
                    a=0;
                    Total.setText("Total   "+a+"yen");
                    textArea1.setText(null);
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("food_ordering_machine");
        frame.setContentPane(new food_ordering_machine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
